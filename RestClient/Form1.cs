﻿using restClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestClient
{
    public partial class Form1 : Form
    {
        string defaultUriText = "http://usa-engapps.extron.com/NEATWebAuthServer/api/Auth/";
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdGo_Click(object sender, EventArgs e)
        {
            RESTClient rClient = new RESTClient();

            rClient.endPoint = txtRequestURI.Text;

            if (rdoBasicAuth.Checked)
            {
                rClient.authType = authenticationType.Basic;
                debugOutput("authenticationType.Basic");
            }
            else
            {
                rClient.authType = authenticationType.NTLM;
                debugOutput("authenticationType.NTLM");
            }

            if (rdoRollOwn.Checked)
            {
                rClient.authTech = autheticationTechnique.RollYourOwn;
                debugOutput("autheticationTechnique.RollYourOwn;");
            }
            else
            {
                rClient.authTech = autheticationTechnique.NetworkCredential;
                debugOutput("autheticationTechnique.NetworkCredential");
            }

            if(txtQuery.Enabled && txtQuery.Text.Length > 0)
            {
                if (rdoEncrypted.Checked)
                {
                    ObjectCrypter2 crypter = new ObjectCrypter2();

                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(txtQuery.Text);
                    byte[] encryptedBytes = crypter.encrypt(bytes);
                    string crypted = Convert.ToBase64String(encryptedBytes);//, Base64FormattingOptions.InsertLineBreaks);
                    byte[] aesBytes = Convert.FromBase64String(crypted);
                    byte[] buffer = crypter.decrypt(aesBytes);
                    string result = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);

                    string queryText = "?task=" + crypted;
                    rClient.endPoint = txtRequestURI.Text + queryText;
                }
                else
                {
                    rClient.endPoint = txtRequestURI.Text + txtQuery.Text;
                }
            }
            else
            {
                rClient.endPoint = txtRequestURI.Text;
            }

            rClient.userName = txtUserName.Text;
            rClient.userPassword = txtPassword.Text;

            //debugOutput("RESTClient Object created.");

            string strJSON = string.Empty;

            strJSON = rClient.makeRequest();

            debugOutput(strJSON);

        }

        private void debugOutput(string strDebugText)
        {
            try
            {
                System.Diagnostics.Debug.Write(strDebugText + Environment.NewLine);
                txtResponse.Text = txtResponse.Text + strDebugText + Environment.NewLine;
                txtResponse.SelectionStart = txtResponse.TextLength;
                txtResponse.ScrollToCaret();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message, ToString() + Environment.NewLine);
            }
        }

        private void comboCommands_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboCommands.Text == string.Empty)
                txtQuery.Text = string.Empty;
            else
                txtQuery.Text = "?task=" + comboCommands.Text;
        }

        private void cmdClear_Click(object sender, EventArgs e)
        {
            txtResponse.Text = "";
        }

        private void txtRequestURI_TextChanged(object sender, EventArgs e)
        {
            txtQuery.Enabled = defaultUriText.ToLower() == txtRequestURI.Text.ToLower();
        }
    }
}
